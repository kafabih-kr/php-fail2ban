#!/usr/bin/env php
<?php
define("FAIL2BAN_DATA", __DIR__ . "/fail2ban");
define("MAX_FAILED", 5);
require __DIR__ . "/src/Fail2Ban/Core.php";
require __DIR__ . "/src/Fail2Ban/AuthLogReader.php";
require __DIR__ . "/src/Fail2Ban/ParseUserLogin.php";
Fail2Ban\Core::run();
