# PHP Fail2Ban

### Under attack and want to install Fail2Ban not using package manager?
PHP Fail2Ban is your solution.



## Kegunaan :
Untuk melindungi server dari SSH Brute Force Login.


## Constants :
`MAX_FAILED` Jumlah maksimal login gagal.

`FAIL2BAN_DATA` Penyimpanan logs dan banned list.


## Installation :
1. Clone git ini atau <a href="https://github.com/kafabih4raka/php-fail2ban/releases/tag/2.0">download disini</a>.
2. Login sebagai root ```sudo -i```.
3. Jalankan ```crontab -e ``` atau ```sudo nano /etc/crontab ```.
4. Tambahkan ```* * * * * php <path folder>fail2ban.php ``` diakhir file.
5. Simpan.


## How it works?
1. PHP Fail2Ban akan membaca file `/var/log/auth.log` atau `/var/log/security` dan menyimpan semua informasi IP, User dan Waktu yang berkaitan dengan SSH Login yang gagal.
2. Kemudian PHP Fail2Ban akan menghitung jumlah gagal login tiap IP dan akan memasukkan IP yang memiliki jumlah gagal login lebih dari atau sama dengan constant `MAX_FAILED` untuk dimasukkan ke dalam banned list.
3. PHP Fail2Ban akan memasukkan IP yang ada di dalam banned list ke `/etc/hosts.deny`.
4. PHP Fail2Ban akan melakukan restart pada service SSH sesudah melakukan perubahan pada `/etc/hosts.deny` agar perubahan dapat terapply pada server.

## Credits
<a href="https://github.com/ammarfaizi2">Ammar Faizi</a>
<a href="https://github.com/kafabih-kr">Rahmat Kafabih</a>

## License
Released under MIT License - see the [License File](LICENSE) for details.
